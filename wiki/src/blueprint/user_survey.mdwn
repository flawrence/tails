[[!meta title="User survey"]]

[[!tails_ticket 14545]]

[[!toc levels=2]]

Questions from the Foundations Team
===================================

Research questions
------------------

### OpenPGP support outside of Thunderbird ([[!tails_ticket 8310]], [[!tails_ticket 17183]], [[!tails_ticket 17169]])

This will become an even hotter topic once Enigmail goes away in a few months:
Thunderbird will maintain its own keyring, independently from GnuPG's ⇒
Seahorse will stop being useful for anything related to email, so the
cost/benefit of vaguely supporting it will become even higher.

- **How popular is OpenPGP inside and outside Thunderbird?**

  OpenPGP is much more popular outside Thunderbird than inside
  Thunderbird:

  - 16% of our users use OpenPGP inside Thunderbird.
  - 49% of our users use OpenPGP outside Thunderbird.

- **How technical are the people using OpenPGP outside Thunderbird?**

  People using OpenPGP outside Thunderbird are slightly less technical
  that people using OpenPGP inside Thunderbird:

  - Enigmail users use mostly Linux significantly more than non-Enigmail
    users: 62% vs. 50%.
  - Enigmail users use the command line slightly more than non-Enigmail
    users: 62% vs. 56%.
  - The most popular OpenPGP tools are Nautilus (41%), Seahorse (37%),
    and the applet (34%).
  - OpenPGP users use mostly Linux slightly more than our users in
    general: 54% vs. 49%.

- **Could these people use Thunderbird instead?**

  No.

  - 33% of our users use OpenPGP only outside Thunderbird.
  - 15% of our users use OpenPGP mostly to communicate using a website.
  - 16% of our users use OpenPGP mostly to communicate by email, which
    is the same fraction as the number of Enigmail users.

- **Who will suffer from the Thunderbird migration because they use
  OpenPGP both inside and outside Thunderbird?**

  Almost all our Enigmail users:

  - 1% of our users only use OpenPGP inside Thunderbird.
  - 16% of our users use OpenPGP both inside and outside Thunderbird.
  - 55% of Enigmail users use OpenPGP mostly to communicate by email.
  - 16% of Enigmail users use OpenPGP mostly to communicate using a
    website.

### Can we stop including an IRC client by default? (#15816)

This open question has been the main blocker for replacing Pidgin for years.

- **What would be the cost of removing the support for IRC by default in Tails?**

  The cost would be way less than breaking Electrum or Seahorse:

  - 8% of our users use IRC in Pidgin.
  - 28% of our users use Electrum.
  - 37% of our users use Seahorse.

  We should provide an alternative, otherwise the noise might be similar
  to dropping 32-bit computers (4%), though the concrete impact is very
  different for people. IRC users are also use Electrum much more than
  our users in general: 51% vs. 28%.

- **How technical are the people using IRC in Tails?**

  IRC users are much more technical than our users in general:

  - IRC users use mostly Linux significantly more than our users in
    general: 63% vs. 49%.
  - IRC users use OpenPGP a lot more than our users in general: 91% vs.
    50%.
  - IRC users use the command line a lot more than our OpenPGP users in
    general: 91% vs. 58%.

- **Are people using IRC in Tails to connect to servers that do not block Tor?**

  The most popular IRC servers are:

  - Freenode: 52%
  - Private IRC servers: 43%
  - IRCnet: 34%
  - OFTC: 10%
  - EFnet: 10%
  - Undernet: 7%

  I didn't check how much these servers block Tor.

- **How popular is XMPP among our users?**

  A little bit more than Enigmail:

  - 16% of our users use Enigmail.
  - 17% of our users use XMPP in Pidgin.
  - 58% of our Pidgin users use only XMPP.
  - 92% of our Pidgin users use XMPP and IRC.

### Notes

- Secure Drop mentions encrypting files with GPG went sending submissions:
  <http://33y6fjyhs3phzfjj.onion/lookup>.

### Prompt

Survey on **ease of use and online privacy tools**

**Contribute to Tails by answering these few questions!** (4&ndash;5 minutes)

To improve Tails we need to learn how people use it but, unlike
others, we don't track you against your will. Your answers are anonymous
and only our team can access the data.

### Questions

- **Overall, how difficult or easy is it for you to use Tails?** (`*`)

  7-point scale from *Very Difficult* to *Very Easy*

- **If you could change just one thing in Tails, what would it be?**

  Short text:

- **How often, if at all, do you use the following tools in Tails?** (`*`)

  Randomized array

| | Never | Sometimes | Most of the time | Don't know | No answer |
|--|
| OpenPGP (also called PGP, GnuPG, or GPG) |
| Pidgin [[!img lib/apps/pidgin.png link="no"]] |
| Thunderbird [[!img lib/apps/thunderbird.png link="no"]] |
| Electrum [[!img lib/apps/electrum.png link="no"]] |
| OnionShare [[!img lib/apps/onionshare.png size="22x22" link="no" ]] |
| Tor Browser [[!img lib/apps/tor-browser.png size="22x22" link="no" ]] |

- **Other than Tails, which of the following operating systems do you use the most?**

  * Windows
  * macOS
  * Linux

### Conditional questions on OpenPGP

- **How often, if at all, do you use the following tools for OpenPGP in Tails?** (`*`)

  Randomized array

| | Never | Sometimes | Most of the time | Don't know |
|--|--|--|--|
| The `gpg` command line [[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/utilities-terminal.png size="22x22" link="no"]] |
| *Enigmail* in *Thunderbird* [[!img doc/first_steps/persistence/thunderbird.png size="22x22" link="no"]] |
| The OpenPGP applet in the top bar [[!img doc/encryption_and_privacy/gpgapplet/gpgapplet_with_text.png size="22x22" link="no"]] |
| The *Passwords and Keys* utility [[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/seahorse.png size="22x22" link="no"]] |
| The *Files* browser [[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/files.png size="22x22" link="no"]] |
| The *Archive* manager [[!img doc/first_steps/introduction_to_gnome_and_the_tails_desktop/file-roller.png size="22x22" link="no"]] |

- **Which other tool, if any, do you use for OpenPGP in Tails?**

  Short text

- **Which of the following options describe the best what you use OpenPGP for?**

  Single choice

  * Exchanging encrypted messages or files by email
  * Exchanging encrypted messages or files using a website
  * Exchanging encrypted messages or files using an external device
  * Encrypting text or files for myself
  * Other:

### Conditional questions on Pidgin

- **How often, if at all, do you use Pidgin in Tails to connect to XMPP (also called Jabber) servers?**

  *To see if your accounts use XMPP/Jabber, choose Accounts → Manage Accounts in Pidgin.*

  * Never
  * Sometimes
  * Most of the time
  * Don't know
  * No answer

- **How often, if at all, do you use Pidgin in Tails to connect to IRC servers?**

  *To see if your accounts use IRC, choose Accounts → Manage Accounts in Pidgin.*

  * Never
  * Sometimes
  * Most of the time
  * Don't know
  * No answer

- **How often, if at all, do you use private conversations (also called OTR) in Pidgin in Tails?**

  * Never
  * Sometimes
  * Most of the time
  * Don't know
  * No answer

- **Which IRC servers, if any, do you connect to using Pidgin in Tails?**

  Randomize multiple choice

  * Private IRC servers
  * Freenode
  * IRCnet
  * Undernet
  * OFTC
  * EFnet
  * Leetnet
  * Other:

  <!-- https://netsplit.de/networks/top10.php -->

### Conditional questions on OpenPGP or Pidgin

- **We might be interested in asking you a few more questions to
  understand better how you use OpenPGP or Pidgin in Tails. If you feel
  like it, you can share your email address with us. We will only use it
  to contact you as part of this research and delete it afterwards.**

  Short text

I2P (#12264)
------------

It's unclear what we should tell nowadays to folks interested in re-adding I2P
support.

<!--

Research questions
==================

- Size of user base

  - How many users do we really have?
  - Right now we only have number of boots per day but combining this
    with how frequently people use Tails, we could extrapolate rough
    number of users and maybe usage categories (fraction of frequent
    users and occasional users, etc.).

- Technical skills

  - How technically skilled are our users?
  - How big is the difference between the technical skills of our target
    audience and real audience?
  - Is there a match between how hard Tails is to use and the skills of
    our actual users?

- Region

  - Is Tails useful and accessible by a global audience?
  - Information on where Tails is used the most is very helpful for
    fundraising, outreach, or translation efforts.

- Current features

  - What are people using Tails the most?
    This would help us clarify what are the most important features of
    Tails and prioritize incremental improvements.

- New features

  - How shall we prioritize our future plans?
  - What is missing the most in Tails?
    This would help us build a better roadmap.

Survey questions
================

- **In which region of the world do you use Tails the most?**

  Single choice:

  * North America
  * Latin America and the Caribbeans
  * Western, Northern, and Southern Europe
  * Eastern Europe and Central Asia
  * Middle-East and North Africa
  * Western, Eastern, Central, and Southern Africa
  * South Asia
  * East and Southeast Asia
  * Oceania

- **Other than Tails, which of the following operating systems do you use the most?** (`*`)

  Single choice:

  - Windows
  - macOS
  - Linux

- **Which of the following tasks are the most important to you when using Tails?** (`*`)

  Pick n/10:

  * Chat on IRC
  * Chat on XMPP/Jabber
  * Read and write email in Tor Browser
  * Read and write emails using Thunderbird
  * Read Atom and RSS feeds using Thunderbird
  * Share file using OnionShare
  * Exchange bitcoins using Electrum
  * Use encrypted storage devices other than the Persistent Storage
  * Encrypt and decrypt PGP messages using Thunderbird
  * Encrypt and decrypt PGP messages outside of Thunderbird
  * Delete files securely
  * Manage passwords using KeePassXC
  * Create or edit office documents
  * Create or edit images
  * Create or edit audio files
  * Create or edit video
  * Print documents on paper
  * Access the internal hard disk of the computer
  * Publish content on the web anonymously
  * Connect to remote servers using SSH
  * Use somebody else's computer
  * Avoid viruses and spyware on my own computer
  * Store sensitive documents in the Persistent Storage
  * Clean metadata on images, video, or document
  * Use the command line
  * Change the security level of Tor Browser
  * Check the circuit view of Tor Browser
  * Clone the current Tails to another USB stick using Tails Installer
  * Other: Short free text

  Discarded:

  * Use Tor bridges or pluggable transports
  * Manage a social media account or a blog under a different identity (but not anonymously)
  * Participate in online communities (forums, chat, etc.) anonymously or under a different identity
  * Use the Unsafe Browser to log in to a captive portal

- **What are your main reasons to use Tails?**

  Single choice:

A. I want to hide information about myself
B. I want to communicate and collaborate securely
C. I want to store information safely
D. I want to leave no trace on the computer
E. I want information to be free
F. I don't want my data to be gathered by corporations and governments

  Personas:

→ Access or publish sensitive or censored information (Riou)
    Access censored information online
    Publish sensitive information
    Access sensitive information
    Store, edit, and anonymize sensitive data
→ Hide information to people around me (Kim)
    Hide information from their family
    Hide their identity
    Avoid raising suspicion
    I want to keep information secret from my family and close people
    I don't want to raise suspicion
→ Avoid government and corporate mass surveillance online (Derya)
    I just want more privacy
    I want to keep information secret from my government
    Avoid corporate and government surveillance
→ Communicate or collaborate with others (Cris)
    Work with others who are surveilled or at risk
    Communicate with known and unknown peers
    Share and work on documents with others
    I want to communicate with others who are under surveillance
→ Have a more secure computer
    Use a computer that is not mine
    Use an untrusted computer
    I need to use a computer that is not mine

    I want to access sensitive information stealthily
    I want to hide my identity
    I want to hide my location
    I want to communicate securely with known peers
    I want to communicate securely with unknown peers
    We want to share and work on documents privately
    I need to safely store my data
    I want to edit or anonymize my data

→ Don't

    Help others access censored information
    Understand people using privacy tools
    I want to understand people using Tails

- **New features**

  Ranking:

- **Overall, how difficult or easy is it for you to use Tails?**

  [Single Ease Question](https://measuringu.com/seq10/)

- **What one things would you improve on our website?**

  Short text:

-->

Resources
=========

- [MeasuringU: How to Conduct a Top Task Analysis](https://measuringu.com/top-tasks/)
- [MeasuringU: 12 Tips For Writing Better Survey Questions](https://measuringu.com/survey-questions/)
- [MeasuringU: 10 Tips For Your Next Survey](https://measuringu.com/survey-tips/)
- [Don A. Dillman et al., Internet, Phone, Mail, and Mixed-Mode Surveys](https://b-ok.cc/book/2735848/c2722f)
- [Norman M. Bradburn et al, Asking Questions: The Definitive Guide to Questionnaire Design](https://b-ok.cc/book/736405/94a5b9)
