# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-07-23 01:14+0000\n"
"PO-Revision-Date: 2019-10-23 11:51+0000\n"
"Last-Translator: Joaquín Serna <bubuanabelas@cryptolab.net>\n"
"Language-Team: Spanish <http://translate.tails.boum.org/projects/tails/"
"overview/es/>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.20\n"

#. type: Content of: <div>
msgid "[[!meta title=\"Install from another Tails\"]]"
msgstr "[[!meta title=\"Instalar desde otro Tails\"]]"

#. type: Content of: outside any tag (error?)
#, fuzzy
#| msgid ""
#| "[[!meta robots=\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel="
#| "\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/assistant"
#| "\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/"
#| "overview\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/"
#| "stylesheets/install-clone\" rel=\"stylesheet\" title=\"\"]] [[!inline "
#| "pages=\"install/inc/overview\" raw=\"yes\" sort=\"age\"]] [["
msgid ""
"[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]] [[!meta "
"stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]] [[!"
"meta stylesheet=\"inc/stylesheets/overview\" rel=\"stylesheet\" title=\"\"]] "
"[[!meta stylesheet=\"inc/stylesheets/install-clone\" rel=\"stylesheet\" "
"title=\"\"]] [[!inline pages=\"install/inc/overview\" raw=\"yes\" sort=\"age"
"\"]] [["
msgstr ""
"[[!meta robots=\"noindex\"]] [[!meta stylesheet=\"bootstrap.min\" rel="
"\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/assistant\" "
"rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/overview"
"\" rel=\"stylesheet\" title=\"\"]] [[!meta stylesheet=\"inc/stylesheets/"
"install-clone\" rel=\"stylesheet\" title=\"\"]] [[!inline pages=\"install/"
"inc/overview.es\" raw=\"yes\" sort=\"age\"]] [["

#. type: Content of: <div><div>
msgid "Let's go!"
msgstr "¡Vamos!"

#. type: Content of: outside any tag (error?)
msgid "|install/clone]]"
msgstr "|install/clone]]"
